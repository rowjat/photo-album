<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentAndRatingRequest;
use App\Model\Album;
use App\Model\CommentAndRating;
use App\Model\Photo;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    /**
     * @var Album
     */
    private $album;
    /**
     * @var Photo
     */
    private $photo;
    /**
     * @var CommentAndRating
     */
    private $commentAndRating;

    public function __construct(Album $album,Photo $photo,CommentAndRating $commentAndRating){

        $this->album = $album;
        $this->photo = $photo;
        $this->commentAndRating = $commentAndRating;
    }
    public function getAlbum(Request $request,$slug){
        if(isset($request->password)){
            $album = $this->album->where('slug',$slug)
                ->where('password',$request->password)
                ->first();
            if($album){
                $photos = $this->photo->where('album_id',$album->id)->paginate(12);
                return view('album',compact('album','photos'));
            }
        }
        return abort(404,'No page found');

    }

    public function commentsAndRating(CommentAndRatingRequest $request){

        $commentAndRating = new $this->commentAndRating;
        $commentAndRating->album_id = $request->album_id;
        $commentAndRating->email = $request->email;
        $commentAndRating->comments = $request->comments;
        $commentAndRating->rating = $request->rating;
        $commentAndRating->save();
return back()->with('success','comment and rating almost done');
    }
}
