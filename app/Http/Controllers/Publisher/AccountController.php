<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Controllers\Controller;
class AccountController extends Controller
{
    public function account(){
        $user = auth()->user();
        return view('publisher.account.index',compact('user'));
    }
}
