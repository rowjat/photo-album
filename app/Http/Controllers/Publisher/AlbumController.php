<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Requests\AlbumForUpdateRequest;
use App\Http\Requests\AlbumRequest;
use App\Http\Controllers\Controller;
use App\Model\Album;
use App\Model\Photo;
use Illuminate\Http\Request;
use Image;
class AlbumController extends Controller
{

    /**
     * @var Album
     */
    private $album;
    /**
     * @var Photo
     */
    private $photo;

    public function __construct(Album $album,Photo $photo){

        $this->album = $album;
        $this->photo = $photo;
    }
    public function index(Request $request){

        $albums = $this->album->where('user_id',auth()->user()->id);
        if(isset($request->search)){
            $albums=$albums->where('name','LIKE',"%$request->search%");
        }
        $albums=$albums->orderBy('created_at','dasc')->paginate(5);
        if(isset($request->search)){
            $albums=$albums->appends(['search'=>$request->search]);
        }


        return view('publisher.album.index',compact('albums'));
    }

    public function store(AlbumRequest $request){
        $file_name ='';
        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $file_name = 'album_'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = 'public/assets/images/albumThumb';
            $img = Image::make($file->getRealPath());
            $img->resize(270, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->crop(270, 220, 0, 0);

            $img->save($destinationPath.'/'.$file_name);
        }

        $album = new $this->album;
        $album->name = $request->name;
        $album->slug = str_slug($request->name);
        $album->description = $request->description;
        $album->cover_image = $file_name;
        $album->password = random_int(111111111,999999999);
        $album->user_id = auth()->user()->id;
        $album->save();
        return back()->with('success','Album Create Successful');
    }
    public function view($id){
        $album =  $this->album->findOrFail($id);
        if($album->user->id != auth()->user()->id){
            return abort(404,'no page found');
        }
        if($album){
            $photos = $this->photo->where('album_id',$album->id)->paginate(12);
            return view('publisher.album.view',compact('album','photos'));
        }else{
           return abort(404,'No Page found');
        }

    }
    public function update(AlbumForUpdateRequest $request,$id){
        $album =  $this->album->findOrFail($id);
        if($album->user->id != auth()->user()->id){
           return abort(404,'no page found');
        }
        if ($request->hasFile('cover_image')) {
            $base_dir = 'public/assets/images';
            if (file_exists($base_dir.'/albumThumb/'.$album->cover_image)) {
                unlink($base_dir.'/albumThumb/'.$album->cover_image);
            }
            $file = $request->file('cover_image');
            $file_name = 'album_'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = 'public/assets/images/albumThumb';
            $img = Image::make($file->getRealPath());
            $img->resize(270, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->crop(270, 220, 0, 0);
            $img->save($destinationPath.'/'.$file_name);

            $album->cover_image = $file_name;
        }
        $album->name = $request->name;
        $album->slug = str_slug($request->name);
        $album->description = $request->description;
        $album->save();
        return back()->with('success','Album update Successful');
    }
    public function delete(Request $request){
//        dd($request->all());
        $album = $this->album->findOrFail($request->album_id);
        $base_dir = 'public/assets/images';
        if($album->cover_image){
            if (file_exists($base_dir.'/albumThumb/'.$album->cover_image)) {
                unlink($base_dir.'/albumThumb/'.$album->cover_image);
            }
        }
        if($album->photo){
           foreach($album->photo as $photo){
               if($photo->photo){
                   if (file_exists($base_dir.'/photo/'.$photo->photo)) {
                       unlink($base_dir.'/photo/'.$photo->photo);
                   }
                   if (file_exists($base_dir.'/photoThumb/'.$photo->photo)) {
                       unlink($base_dir.'/photoThumb/'.$photo->photo);
                   }
               }
               $photo->delete();
           }
        }
        $album->delete();
        return redirect()->back()->with('success','delete successful');
    }
}
