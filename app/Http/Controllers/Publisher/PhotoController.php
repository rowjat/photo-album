<?php

namespace App\Http\Controllers\Publisher;

use App\Http\Requests\PhotoForUpdateRequest;
use App\Http\Requests\PhotoRequest;
use App\Model\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
class PhotoController extends Controller
{
    /**
     * @var Photo
     */
    private $photo;

    public function __construct(Photo $photo){

        $this->photo = $photo;
    }
    public function store(PhotoRequest $request,$id){
        $file_name ='';
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $file_name = 'album_'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = 'public/assets/images/photoThumb';
            $img = Image::make($file->getRealPath());
            $img->resize(null, 139, function ($constraint) {
                $constraint->aspectRatio();
            });
            if($img->height()>139){
                $img->crop(170, 139, 0, 0);
            }
            $img->save($destinationPath.'/'.$file_name);
            $destinationPath = 'public/assets/images/photo';
            $file->move($destinationPath, $file_name);
        }

        $photo = new $this->photo;
        $photo->album_id = $id;
        $photo->title = $request->title;
        $photo->description = $request->description;
        $photo->photo = $file_name;
        $photo->save();
        return back()->with('success','Photo Create Successful');
    }
    public function update(PhotoForUpdateRequest $request,$id){
        $photo = $this->photo->findOrFail($id);
        if($photo->album->user->id == auth()->user()->id){
            return abort(404,'no page found');
        }
        $base_dir = 'public/assets/images';
        if ($request->hasFile('photo')) {
            if (file_exists($base_dir.'/photo/'.$photo->photo)) {
                unlink($base_dir.'/photo/'.$photo->photo);
            }
            if (file_exists($base_dir.'/photoThumb/'.$photo->photo)) {
                unlink($base_dir.'/photoThumb/'.$photo->photo);
            }
            $file = $request->file('photo');
            $file_name = 'album_'.time().'.'.$file->getClientOriginalExtension();
            $destinationPath = 'public/assets/images/photoThumb';
            $img = Image::make($file->getRealPath());
            $img->resize(null, 139, function ($constraint) {
                $constraint->aspectRatio();
            });
            if($img->height()>139){
                $img->crop(170, 139, 0, 0);
            }
            $img->save($destinationPath.'/'.$file_name);
            $destinationPath = 'public/assets/images/photo';
            $file->move($destinationPath, $file_name);

            $photo->photo = $file_name;
        }
        $photo->title = $request->title;
        $photo->description = $request->description;

        $photo->save();
        return back()->with('success','Photo Create Successful');
    }
    public function delete(Request $request){
        $photo = $this->photo->findOrFail($request->photo_id);
        $base_dir = 'public/assets/images';
        if($photo->photo){
            if (file_exists($base_dir.'/photo/'.$photo->photo)) {
                unlink($base_dir.'/photo/'.$photo->photo);
            }
            if (file_exists($base_dir.'/photoThumb/'.$photo->photo)) {
                unlink($base_dir.'/photoThumb/'.$photo->photo);
            }
        }
        $photo->delete();
        return redirect()->back()->with('success','delete successful');
    }
}
