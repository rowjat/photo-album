<?php

namespace App;

use App\Model\Album;
use App\Model\Photo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function album(){
        return $this->hasMany(Album::class,'user_id');
    }
    public function photo()
    {
        return $this->hasManyThrough(Photo::class, Album::class);
    }
}
