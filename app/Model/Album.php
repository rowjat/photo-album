<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    public $rating_attr =[1=>0,2=>0,3=>0,4=>0,5=>0];
    /*public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = str_slug($model->name);
        });
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }*/

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function photo(){
        return $this->hasMany(Photo::class,'album_id');
    }

    public function commentAndRating(){
        return $this->hasMany(CommentAndRating::class,'album_id');
    }
    public function ratingCal(){
        $avarage_rating = 0;
        $total_rating = 0;
        for($i=1;$i<=5;$i++){
            $value =$this->commentAndRating->where('rating',$i)->count();
            $total_rating+= $value;
            $avarage_rating+=$value*$i;
        }

       return round($avarage_rating>0?$avarage_rating/$total_rating:0,2);
    }
}
