<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CommentAndRating extends Model
{
    public function album(){
        return $this->belongsTo(Album::class,'album_id');
    }
}
