<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', 'HomeController@index');

    // Authentication Routes...
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');


    Route::get('share-album/{slug}','AlbumController@getAlbum');
    Route::post('comments-rating','AlbumController@commentsAndRating');

Route::middleware(['publisher'])->namespace('Publisher')->group(function () {
    //album
    Route::get('album','AlbumController@index');
    Route::post('album','AlbumController@store');
    Route::get('album/{id}/view','AlbumController@view');
    Route::post('album/{id}/update','AlbumController@update');
    Route::post('album/delete','AlbumController@delete');
    //photo
    Route::post('photo/{album_id}/create','PhotoController@store');
    Route::post('photo/{id}/update','PhotoController@update');
    Route::post('photo/delete','PhotoController@delete');
    //account
    Route::get('account','AccountController@account');
});



