@extends('master')
@section('title','| Home')
@section('content')
<div class="span8">
    <div class="flexslider">
        <ul class="slides">
            <li><a href="#"><img src="{{asset('public/assets/images/slider/slider-1.jpg')}}" style="height: 370px" alt="slider" /></a></li>
            <li><a href="#"><img src="{{asset('public/assets/images/slider/slider-2.jpg')}}" style="height: 370px" alt="slider" /></a></li>
            <li><a href="#"><img src="{{asset('public/assets/images/slider/slider-3.jpg')}}" style="height: 370px" alt="slider" /></a></li>
            <li><a href="#"><img src="{{asset('public/assets/images/slider/slider-4.jpg')}}" style="height: 370px" alt="slider" /></a></li>
            <li><a href="#"><img src="{{asset('public/assets/images/slider/slider-5.jpg')}}" style="height: 370px" alt="slider" /></a></li>
        </ul>
    </div>
</div>
<div class="span4">
    <div class="" id="login" style="text-align: center">
        <h3 style="text-align: center">Login</h3>
        <form action="{{url('login')}}" method="post" class="text-center" id="login_form" >
            {{csrf_field()}}
            <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
            <button type="submit" class="btn btn-inverse">Login</button>
            </div>
            <hr/>
        </form>
        <div style="text-align: center">
            <a href="#" class="action" tag="forget"><i class="fa fa-arrow-left"></i>Forget my password</a>
            | <a href="#" class="action" tag="register">Want to register <i class="fa fa-arrow-right"></i></a>
        </div>
        <br/>
    </div>
    <!--Register-->
    <div class="hide" id="register" style="text-align: center">
        <h3 style="text-align: center">Register</h3>
        <form action="{{url('register')}}" method="post" class="" id="register_form">
            {{csrf_field()}}
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="confirm_password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-inverse">Register</button>
            </div>
        </form>
        <br/>
        <a href="#" class="action" tag="forget"><i class="fa fa-arrow-left"></i> Forget password</a>
        | <a href="#" class="action" tag="login">Login <i class="fa fa-arrow-right"></i></a>
    </div>
    <!--forget_password-->
    <div class="hide" id="forget_password" style="text-align: center">
        <h6 style="text-align: center">Forget password</h6>
        <form action="{{url('password/email')}}" method="post" class="">
            {{csrf_field()}}
            <div class="form-group">
                <input type="email" name="email" value="{{ old('email') }}" placeholder="Email">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-inverse">Send</button>
            </div>
        </form>
        <br/>
        <a href="#" class="action" tag="login"><i class="fa fa-arrow-left"></i> Login</a>
        | <a href="#" class="action" tag="register">Want to register <i class="fa fa-arrow-right"></i></a>
    </div>

</div>
    @endsection
@section('js')
    <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\UserRequest', '#register_form') !!}
    {!! JsValidator::formRequest('App\Http\Requests\AuthRequest', '#login_form') !!}
    <script>
        $(document).ready(function(){
            $(document).on('click','.action',function(e){
                e.preventDefault();
                var action = $(this).attr('tag');
                if(action==='login'){
                    $('#login').removeClass('hide');
                    $('#register').addClass('hide');
                    $('#forget_password').addClass('hide');
                }else if(action==='register'){
                    $('#login').addClass('hide');
                    $('#register').removeClass('hide');
                    $('#forget_password').addClass('hide');
                }else if(action==='forget'){
                    $('#login').addClass('hide');
                    $('#register').addClass('hide');
                    $('#forget_password').removeClass('hide');
                }

            });

            $("#btn-blog-next").click(function () {
                $('#blogCarousel').carousel('next')
            });
            $("#btn-blog-prev").click(function () {
                $('#blogCarousel').carousel('prev')
            });

            $("#btn-client-next").click(function () {
                $('#clientCarousel').carousel('next')
            });
            $("#btn-client-prev").click(function () {
                $('#clientCarousel').carousel('prev')
            });

        });

        $(window).load(function(){

            $('.flexslider').flexslider({
                animation: "slide",
                slideshow: true,
                start: function(slider){
                    $('body').removeClass('loading');
                }
            });
        });

    </script>
    @endsection