@extends('master')
@section('title','| Album - '.$album->slug)
@section('content')
    <div class="span8 gallery">

        <div class="row clearfix">
            <ul class="gallery-post-grid holder">

                @forelse($photos as $photo)
                    @php
                    $image = $photo->photo==null?'default.jpg':$photo->photo;
                    @endphp
                    <li  class="span2 gallery-item" data-id="id-1" data-type="illustration">
                        <span class="gallery-hover-6col hidden-phone hidden-tablet">
                            <span class="gallery-icons">
                                <a href="{{asset('public/assets/images/photo/'.$image)}}" class="item-zoom-link lightbox" title="{{$photo->description}}" data-rel="prettyPhoto"></a>
                                {{--<a href="gallery-single.htm" class="item-details-link"></a>--}}
                            </span>
                        </span>
                        <a href="#"><img src="{{asset('public/assets/images/photoThumb/'.$image)}}" alt="Gallery"></a>
                        <span class="project-details"><a href="#">{{$photo->title}}</a></span>
                       </li>

                @empty
                    <h3>No result found !</h3>
                @endforelse

            </ul>
        </div>

        {{ $photos->links('pagination.default') }}

    </div><!-- End gallery list-->
    <div class="span4" style="margin-bottom: 20px">
        <h2>{{$album->name}}</h2>
        <i class="icon-user"></i> <a href="#">{{$album->user->username}}</a>
        <p>{{$album->description}}</p>
        <i class="icon-calendar"></i> {{date('d/m/Y',strtotime($album->created_at))}} |
        <i class="icon-comment"></i> <a href="#">{{$album->commentAndRating?$album->commentAndRating->count():0}} Comments</a> |
        <i class="fa fa-star"></i> <a href="#">{{$album->ratingCal()}}</a>

        <hr/>
        <!--<br/>-->
        <form action="{{url('comments-rating')}}" method="post" id="comments_rating_form" style="text-align: right">
            {{csrf_field()}}
            <div class="form-group">
            <input type="hidden" name="album_id"  value="{{$album->id}}" class="form-control" placeholder="email">
            <input type="email" name="email" class="form-control" placeholder="email">
            </div>
            <div class="form-group">
            <textarea name="comments" placeholder="comments" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <select name="rating" class="form-control">
                    <option value="">Rating</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-inverse pull-right" type="submit">Submit</button>
            </div>
        </form>

        <br/>
        <hr/>
        <style>
            ul#comment{
                margin: 0px auto;
            }
            ul#comment li{
                list-style: none;
                border-bottom: 1px dotted white;
            }
            ul#comment li p{
                padding: 0px 10px;
                margin: 0px;
            }
            ul#comment li p.content{
                border-left:4px solid #dafddb
            }
            ul#comment li .comment_history{
                text-align: right;
                padding: 0px;
                margin: 0px;
                color: #972129;
                font-size: 8px;
            }
        </style>
        <div style="height: 200px;overflow: auto;background: #eaeaea;border: 1px solid white;padding: 10px">
            <ul id="comment">
                @forelse($album->commentAndRating as $value)
                <li>
                    <p class="content"><i class="icon-comment"></i> {{$value->comments}}</p>
                    <p class="comment_history">
                        {{--<i class=" fa fa-envelope"></i> {{$value->email}} &nbsp; | &nbsp--}}
                        @for($i=5;$i >= 1;$i--)
                            @if($i > $value->rating)
                        <i class=" fa fa-star-o"></i>
                            @else
                                <i class=" fa fa-star"></i>
                                @endif
                        @endfor
                    </p>
                </li>
                @empty
                    <li><p class="content">no comments and rating</p></li>
                @endforelse
            </ul>
        </div>
    </div>
    @endsection
@section('js')
    <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CommentAndRatingRequest', '#comments_rating_form') !!}
@endsection