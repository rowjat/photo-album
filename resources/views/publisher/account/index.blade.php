@extends('master')
@section('title','| Account -'.$user->username)
@section('content')
    <div class="span12">
        <h3>My account</h3>

        <p>Username : {{$user->username}}</p>
        <p>Email : {{$user->email}}</p>
    </div>
    @endsection