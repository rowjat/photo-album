<div class="modal fade" id="exampleModal_{{$album->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #efe2ec;
background-image: url('https://www.transparenttextures.com/patterns/black-thread-light.png')">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Album ( {{$album->name}} )</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('album/'.$album->id.'/update')}}"  method="post" id="edit_form_{{$album->id}}" enctype="multipart/form-data" style="text-align: center">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" value="{{$album->name}}" placeholder="Album Name">
                    </div>
                    <div class="form-group">
                        <textarea name="description" class="form-control"  placeholder="Description">{{$album->description}}</textarea>
                    </div>
                    <div class="form-group" style="border: 1px dotted gray">
                        <label>Cover Image <span class="help-block">( 270 x 220 )</span></label><input name="cover_image" type="file" placeholder="Cover Image">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="$('#edit_form'+'_'+'{{$album->id}}').submit()">Update</button>
            </div>
        </div>
    </div>
</div>
{!! JsValidator::formRequest('App\Http\Requests\AlbumForUpdateRequest', '#edit_form_'.$album->id) !!}