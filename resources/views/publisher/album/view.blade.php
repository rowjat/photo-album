@extends('master')
@section('title')
    | album #{{$album->name}}
@endsection
@section('content')

    <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <div class="span8 gallery">

        <div class="row clearfix">
            <ul class="gallery-post-grid holder">

                @forelse($photos as $photo)
                    @php
                    $image = $photo->photo==null?'default.jpg':$photo->photo;
                    @endphp
                    @include('publisher.album._partials._edit-photo-form',['data'=>$photo])
                    <li  class="span2 gallery-item" data-id="id-1" data-type="illustration" style="width: 170px;background: white">
                        <span class="gallery-hover-6col hidden-phone hidden-tablet">
                            <span class="gallery-icons">
                                <a href="{{asset('public/assets/images/photo/'.$image)}}" class="item-zoom-link lightbox" title="{{$photo->description}}" data-rel="prettyPhoto"></a>
                                {{--<a href="gallery-single.htm" class="item-details-link"></a>--}}
                            </span>
                        </span>
                        <a href="#" ><img src="{{asset('public/assets/images/photoThumb/'.$image)}}" alt="Gallery"></a>
                        <span class="project-details"><a href="#">{{$photo->title}}</a></span>
                        <span style="float: right"><a href="#" data-toggle="modal" data-target="#exampleModal_{{$photo->id}}"><i class="fa fa-edit"></i></a> | <a href="#" onclick="$('#delete_form'+'_'+'{{$photo->id}}').submit()"><i class="fa fa-trash"></i></a></span>
                    </li>
                    <form action="{{url('photo/delete')}}" onsubmit="return confirm('Do you really want to delete the photo?');"  method="post" id="delete_form_{{$photo->id}}">
                       {{csrf_field()}}
                        <input type="hidden" name="photo_id" value="{{$photo->id}}">

                        </form>
                @empty
                    <h3>No result found !</h3>
                @endforelse

            </ul>
        </div>

        {{ $photos->links('pagination.default') }}

    </div><!-- End gallery list-->
    <div class="span4">
        <h2>{{$album->name}}</h2>
        <i class="icon-calendar"></i> {{date('d/m/Y',strtotime($album->created_at))}} |
        <i class="icon-comment"></i> <a href="#">{{$album->commentAndRating?$album->commentAndRating->count():0}} Comments</a> |
        <i class="fa fa-star"></i> <a href="#">{{$album->ratingCal()}}</a>
        <hr/>
        <p style="position: relative">
            <a href="#" class="js-textareacopybtn" style="position: absolute;top: 0px;left:0px" ><i style="padding: 2px 5px" class="btn btn-info fa fa-clipboard"></i> click this for copy link and Share any where</a>
            <textarea class="js-copytextarea" readonly style="border: 0px solid;width: 80%;height:50px;padding: 20px;background: #eaeaea">{{url('share-album/'.$album->slug).'?password='.$album->password}}</textarea>
        </p>
        <p>{{$album->description}}</p>


        <section class="">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#new" data-toggle="tab"><i class="fa fa-plus"></i> Upload new photo</a></li>
                <li class=""><a href="#info" data-toggle="tab" class="">Comments</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane" id="info">

                    <style>
                        ul#comment{
                            margin: 0px auto;
                        }
                        ul#comment li{
                            list-style: none;
                            border-bottom: 1px dotted white;
                        }
                        ul#comment li p{
                            padding: 0px 10px;
                            margin: 0px;
                        }
                        ul#comment li p.content{
                            border-left:4px solid #eaeaea
                        }
                        ul#comment li .comment_history{
                            text-align: right;
                            padding: 0px;
                            margin: 0px;
                            color: #972129;
                            font-size: 8px;
                        }
                    </style>
                    <div style="height: 200px;overflow: auto;background: #eaeaea;border: 1px solid white;padding: 10px">
                        <ul id="comment">
                            @forelse($album->commentAndRating as $value)
                                <li>
                                    <p class="content"><i class="icon-comment"></i> {{$value->comments}}</p>
                                    <p class="comment_history">
                                        {{--<i class=" fa fa-envelope"></i> {{$value->email}} &nbsp; | &nbsp--}}
                                        @for($i=5;$i >= 1;$i--)
                                            @if($i > $value->rating)
                                                <i class=" fa fa-star-o"></i>
                                            @else
                                                <i class=" fa fa-star"></i>
                                            @endif
                                        @endfor
                                    </p>
                                </li>
                            @empty
                                <li><p class="content">no comments and rating</p></li>
                            @endforelse
                        </ul>
                    </div>
                </div>

                <div class="tab-pane active" id="new">
                    <form action="{{url('photo/'.$album->id.'/create')}}" method="post" id="photo_form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">

                            <input type="text" name="title" class="form-control" placeholder="Title">  <br/>
                        </div>
                        <div class="form-group">

                            <textarea name="description" placeholder="Description"></textarea><br/>
                        </div>
                        <div class="form-group">
                        <label>Image</label><input name="photo" type="file" placeholder="Cover Image"><br/><br/>

                        </div>
                        <div class="fprm-group">

                            <button type="submit" class="btn btn-inverse">Upload Image</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    @endsection
@section('js')
{{--    <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>--}}
    {!! JsValidator::formRequest('App\Http\Requests\PhotoRequest', '#photo_form') !!}
    <script>
        var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

        copyTextareaBtn.addEventListener('click', function(event) {
            var copyTextarea = document.querySelector('.js-copytextarea');
            copyTextarea.focus();
            copyTextarea.select();

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }
        });


    </script>
@endsection