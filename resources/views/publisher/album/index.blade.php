@extends('master')
@section('title','| Album')
@section('content')
    <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <div class="span8 blog">


        <table style="width: 100%">
        <tr>
            <td>{{ $albums->links('pagination.default') }}</td>
            <td>
                <form class="form-search pull-right form-inline" action="{{url('/album')}}">
                    <div class="form-group">
                        <div class="input-append ">
                            <input type="text" class="span2 search-query" name="search" placeholder="Search album name">
                            <button type="submit" class="btn">Search</button>
                        </div>
                    </div>

                </form>
            </td>
        </tr>
        </table>
        @forelse($albums as $album)
            @php
            $cover_image = $album->cover_image==null?'gallery-img-1-4col.jpg':$album->cover_image
            @endphp
@include('publisher.album._partials._edit-album-form',['data'=>$album])
            <article class="clearfix" style="position: relative;" >
                <div class="btn-group" style="position: absolute    ;left: 0px;">
                    <a href="{{url('album/'.$album->id.'/view')}}" class="btn btn-xs btn-default" style="padding: 2px;border-radius: 0px    "><i class="fa fa-eye"></i></a>
                    <a href="#" class="btn btn-xs btn-default" data-toggle="modal" data-target="#exampleModal_{{$album->id}}" style="padding: 2px"><i class="fa fa-edit"></i></a>
                    <a href="#" class="btn btn-xs btn-danger" onclick="$('#delete_form'+'_'+'{{$album->id}}').submit()" style="padding: 2px;border-radius: 0px"><i class="fa fa-trash"></i></a>
                </div>
                <a href="{{url('album/'.$album->id.'/view')}}" style="background: red;width: 300px"><img src="{{asset('public/assets/images/albumThumb/'.$cover_image)}}" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="{{url('album/'.$album->id.'/view')}}">{{$album->name}}</a></h4>
                <p style="padding: 0px;margin: 0px">
                    <i class="icon-calendar"></i> {{date('d-m-Y',strtotime($album->created_at))}} |
                    <i class="icon-comment"></i> <a href="#">{{$album->commentAndRating->count()}} Comments</a> |
                    <i class="fa fa-star"></i> <a href="#">{{$album->ratingCal()}} rating</a> |
                    <i class="fa fa-photo"></i> <a href="{{url('album/'.$album->id.'/view')}}">{{$album->photo->count()}} Photo</a>
                </p>


    <form action="{{url('album/delete')}}" onsubmit="return confirm('Do you really want to delete the album?');"  method="post" id="delete_form_{{$album->id}}">
        {{csrf_field()}}
        <input type="hidden" name="album_id" value="{{$album->id}}">
    </form>
                {{--<button class="btn btn-mini btn-inverse" type="button">Read more</button>--}}

                <p style="max-height: 80px;overflow: auto">{{$album->description}}</p>
            </article>


        @empty
            <h3>No Result Found ! </h3>
        @endforelse
        {{ $albums->links('pagination.default') }}

    </div>

    <div class="span3">
        <section class="">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#info" data-toggle="tab">Info</a></li>
                <li><a href="#new" data-toggle="tab"><i class="fa fa-plus"></i> Create new</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="info">
                    <table>
                        <tr><th style="text-align: left">Username :</th><td>{{auth()->user()->username}}</td></tr>
                        <tr><th style="text-align: left">Email :</th><td>{{auth()->user()->email}}</td></tr>
                        <tr><th style="text-align: left">Total Album :</th><td>{{auth()->user()->album->count()}}</td></tr>
                        <tr><th style="text-align: left">Total Photo :</th><td>{{auth()->user()->photo->count()}}</td></tr>
                    </table>
                </div>
                <div class="tab-pane" id="new">
                    <form action="{{url('album')}}" method="post" id="album_form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Album Name">
                        </div>
                        <div class="form-group">
                        <textarea name="description" class="form-control" placeholder="Description"></textarea>
                        </div>
                        <div class="form-group">
                        <label>Cover Image ( 270 x 220 )</label><input name="cover_image" type="file" placeholder="Cover Image">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-inverse">Create New Album</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>



    </div>
    @endsection
@section('js')

    {!! JsValidator::formRequest('App\Http\Requests\AlbumRequest', '#album_form') !!}
    @endsection