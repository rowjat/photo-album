
<script src="{{asset('public/assets/js/bootstrap.js')}}"></script>
<script src="{{asset('public/assets/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('public/assets/js/jquery.flexslider.js')}}"></script>
<script src="{{asset('public/assets/js/jquery.custom.js')}}"></script>
@yield('js')