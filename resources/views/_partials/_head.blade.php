<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{{config('app.name')}} @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS
    ================================================== -->
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap-responsive.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/css/prettyPhoto.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/css/flexslider.css')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/css/custom-styles.css')}}">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" href="{{asset('assets/css/style-ie.css')}}"/>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{asset('public/assets/img/favicon.ico')}}">
    <link rel="apple-touch-icon" href="{{asset('public/assets/img/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('public/assets/img/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('public/assets/img/apple-touch-icon-114x114.png')}}">
    <script src="{{asset('public/assets/js/jquery.min.js')}}"></script>
    <!-- JS
    ================================================== -->
    <!--<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>-->
    <style>
        /*.has-success > span.error-help-block{
            display:none;
        }*/
        .has-error > span.error-help-block{
            /*background: red;*/
            color: red;

            /*display:inline-block;*/
        }
    </style>
</head>