@if(Session::has('success')) 
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>

    <i class="ace-icon fa fa-check green"></i>


    {{Session::get('success')}}
</div>
@endif
@if(Session::has('error')) 
<div class="alert alert-block alert-danger">
    <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>

    <i class="ace-icon fa fa-check green"></i>


    {{Session::get('error')}}
</div>
@endif

@if(count($errors)>0)
<div class="alert alert-block alert-danger">
    <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>


    @foreach($errors->all() as $error)
    {{$error}} <br/>
    @endforeach
</div>


@endif

