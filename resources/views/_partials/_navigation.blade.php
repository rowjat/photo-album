<div class="span7 navigation">
    <div class="navbar">

        <ul class="nav">
            <li class="{{ Request::path() == 'album' ? 'active' : '' }}"><a href="{{url('/album')}}">Album</a></li>
{{--            <li class="{{ Request::path() == 'account' ? 'active' : '' }}"><a href="{{url('account')}}">Account</a></li>--}}
            <li class="dropdown {{ Request::path() == 'account' ? 'active' : '' }}" >
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{auth()->check()?auth()->user()->username:'Account'}} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{url('account')}}">Profile</a>
                    <li><a href="#" onclick="$('#logout_form').submit()">Logout</a>
                </ul>
            </li>

            </li>
        </ul>

    </div>
    <form action="{{url('logout')}}" method="post" id="logout_form">
        {{csrf_field()}}
    </form>

</div>