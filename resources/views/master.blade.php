<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
@include('_partials._head')
<body class="home">
<div class="color-bar-1"></div>
<div class="color-bar-2 color-bg"></div>
<div class="container">
    <div class="row header">
        @include('_partials._logo')
        @if(auth()->check())
        @include('_partials._navigation')
            @endif
    </div>
    <div class="row headline" style="min-height:70vh">
        <div class="span6 pull-right">

            @include('_partials._msg')
        </div>
@yield('content')
    </div>
</div>
@include('_partials._footer')
<div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>
@include('_partials._script')
</body>
</html>
