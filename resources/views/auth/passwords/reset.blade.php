@extends('master')
@section('title','| Reset password')
@section('content')
    <div class="span12" style="text-align: center">
        <form action="{{url('password/reset')}}" method="post" id="reset_form">
            {{csrf_field()}}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="email">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-inverse">Reset Your password</button>
            </div>
        </form>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\ResetPasswordRequest', '#reset_form') !!}
    @endsection